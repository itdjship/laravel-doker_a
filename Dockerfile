FROM telkomindonesia/debian:php-7.2-nginx-novol

WORKDIR /var/www/html

COPY composer.* ./

RUN composer install --no-scripts --no-autoloader

#RUN apt-get update && apt-get install -y

COPY . .

USER root

RUN chmod -R 775 /var/www/html

RUN apt-get update; \
    apt-get install -y --no-install-recommends \
    mysql-client \
    expect



#USER user
ADD init.sh .

RUN ls /var/www/html

RUN ["chmod", "+x", "/var/www/html/init.sh"]

RUN ["sh", "/var/www/html/init.sh"]
