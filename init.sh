#!/bin/sh

echo "Replace default nginx configuration"
mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.orgx
cp /var/www/html/setup_custom/nginx/default.conf /etc/nginx/sites-available/default

echo "Replace default php.ini configuration"
cp /var/www/html/setup_custom/php/php.ini /etc/php/7.2/cli/php.ini

echo "Replace default supervisord configuration"
#cp /var/www/html/setup_custom/supervisor/supervisord.conf /var/www/etc/supervisor/supervisord.conf

echo "Add supervisor email queue configuration"
#cp /var/www/html/setup_custom/supervisor/03-queue-email.conf /var/www/html/storage/supervisor/conf.d/03-queue-email.conf

echo "Create storage directory"
mkdir -p /var/www/html/bootstrap/cache/ \
         /var/www/html/storage/app \
         /var/www/html/storage/logs \
         /var/www/html/storage/tmp \
         /var/www/html/storage/framework/sessions \
         /var/www/html/storage/framework/views \
         /var/www/html/storage/framework/cache \
         /var/www/html/storage/fonts/ \
&& chmod -R 775 /var/www/html/storage/ \
&& chmod -R 777 /var/www/html/storage/tmp/ \
&& chmod -R 777 /var/www/html/storage/fonts/ \
&& chmod -R 775 /var/www/html/bootstrap/cache/ \
&& chown -R $user:$user /var/www/html

echo "Run composer optimize"
composer dump-autoload --optimize
#sleep 30
#echo "Run composer predis"
#composer require predis/predis


echo "Ceck DB"


#MYSQL_ROOT_PASSWORD=hello12345
#SECURE_MYSQL=$(expect -c "
#set timeout 10
#spawn mysql_secure_installation
#expect \"Enter current password for root (enter for none):\"
#send \"$MYSQL\r\"
#expect \"Change the root password?\"
#send \"n\r\"
#expect \"Remove anonymous users?\"
#send \"y\r\"
#expect \"Disallow root login remotely?\"
#send \"y\r\"
#expect \"Remove test database and access to it?\"
#send \"y\r\"
#expect \"Reload privilege tables now?\"
#send \"y\r\"
#expect eof
#")

#echo "$SECURE_MYSQL"

apt-get -y purge expect
echo "check DB"
mysql -h mysql -e "show databases;";

echo "Run db migration"
#echo mysql;
#echo odissey-v2-58_mysql_1;
#sleep ${LARAVEL_SEED_SLEEP}
sleep 10
nc -vz mysql 3306
php artisan migrate --force

echo "Run default entrypoint"
#/usr/bin/supervisord -n -c /var/www/etc/supervisor/supervisord.conf -d /var/www/etc/supervisor
